Project Title <br/>  
Student test task

Prerequisites <br/>
For getting all neded libraries just use
`pip install -r requirements.txt `
from root directory


Running development server <br/>
For running dev server just use
`python manage.py runserver`
from root directory

Running the tests <br/>
For start API test just use
`python manage.py test`
from root directory

Endpoints<br/>
http://127.0.0.1:8000/api/v1/test_task/course - get curses list. Also you can create new course instance here.<br/>
http://127.0.0.1:8000/api/v1/test_task/course/detail/1/ - get detail view of course with pk = 1. Also, this view allow put, putch and delete methods.<br/>
http://127.0.0.1:8000/api/v1/test_task/student - get students list. Also you can create new student instance here.<br/>
http://127.0.0.1:8000/api/v1/test_task/student/detail/1/ - get detail view of studednt with pk = 1. Also, this view allow put, putch and delete methods.<br/>
http://127.0.0.1:8000/api/v1/test_task/student/assign_student_to_course - this view resposible for create new course participant instance.<br/>
http://127.0.0.1:8000/api/v1/test_task/student/assigned_to_course - get assigned to course students list. 
http://127.0.0.1:8000/api/v1/test_task/student/unassign_from_course/4/ - this view resposible for delete course participant instance by it's pk.<br/>
http://127.0.0.1:8000/api/v1/test_task/student/report/1 - get report in csv format.

