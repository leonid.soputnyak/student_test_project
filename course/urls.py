from django.urls import path
from .views import (
    CourseAPIView, CourseAPIDetailView,
)

app_name = 'course'
urlpatterns = [
    path(
        'course',
        CourseAPIView.as_view(),
        name='list'
    ),
    path(
        'course/detail/<int:pk>/',
        CourseAPIDetailView.as_view(),
        name='detail'
    ),
]
