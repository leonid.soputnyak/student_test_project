from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from .models import (
    Course, CourseParticipant,
)


class CourseSerializer(serializers.ModelSerializer):
    participant_students_count = serializers.SerializerMethodField()

    class Meta:
        model = Course
        fields = [
            'id',
            'name',
            'start_date',
            'end_date',
            'participant_students_count'
        ]
        validators = [
            UniqueTogetherValidator(
                queryset=Course.objects.all(),
                fields=(
                    'name',
                ),
                message='Field Name should be uniq'
            )
        ]

    @staticmethod
    def get_participant_students_count(obj):
        return CourseParticipant.objects.filter(course=obj).count()
