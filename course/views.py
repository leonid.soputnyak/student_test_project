from rest_framework import generics, mixins
from .models import (
    Course
)
from .serializers import (
    CourseSerializer
)


class CourseAPIView(
    mixins.CreateModelMixin,
    generics.ListAPIView
):
    """
    General view which responsible for getting a courses list
    and creating a new course instance

    post:
    Create a new course instance.
    """
    serializer_class = CourseSerializer

    def get_queryset(self):
        query_set = Course.objects.all()
        return query_set

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class CourseAPIDetailView(
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.RetrieveAPIView,
):
    """
    Detail views which responsible for
    read, update and delete  a course instance.

    put:
    Update existing course instance.
    \n

    patch:
    Update existing course instance.
    \n

    delete:
    Delete existing course instance.
    """

    serializer_class = CourseSerializer

    def get_queryset(self):
        query_set = Course.objects.all()
        return query_set

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
