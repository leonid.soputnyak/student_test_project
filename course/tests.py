from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from course.models import Course


class CourseAPITestCase(APITestCase):
    def setUp(self):
        course_obj = Course.objects.create(
            name='test_course',
            description='test_description',
        )

    def create_item(self):
        url = api_reverse('course:list')
        data = {
            'name': 'some_course_for_test'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Course.objects.count(), 2)
        return response.data

    def test_course_create(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('course:detail', kwargs={'pk': data_id})
        rud_data = {
            'name': 'some_new_course_for_test'
        }

        get_response = self.client.get(rud_url, rud_data, format='json')
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)

    def test_course_update(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('course:detail', kwargs={'pk': data_id})
        rud_data = {
            'name': 'some_new_course_for_test'
        }

        put_response = self.client.put(rud_url, rud_data, format='json')
        self.assertEqual(put_response.status_code, status.HTTP_200_OK)

    def test_course_delete(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('course:detail', kwargs={'pk': data_id})
        rud_data = {
            'name': 'some_new_course_for_test'
        }

        delete_response = self.client.delete(rud_url, rud_data, format='json')
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        get_response = self.client.delete(rud_url, format='json')
        self.assertEqual(get_response.status_code, status.HTTP_404_NOT_FOUND)
