from django.urls import path
from .views import (
    StudentAPIView, StudentAPIDetailView, StudentAPIAssignedToTheCourseView,
    StudentAPIAssignToCourse, StudentAPIUnassignedFromTheCourseView, StudentAPIPerformanceReport
)

app_name = 'student'

urlpatterns = [
    path(
        'student', StudentAPIView.as_view(),
        name='list'
    ),
    path(
        'student/detail/<int:pk>/',
        StudentAPIDetailView.as_view(),
        name='detail'
    ),
    path(
        'student/assign_student_to_course',
        StudentAPIAssignToCourse.as_view(),
        name='assign_student_to_course'
    ),
    path(
        'student/assigned_to_course',
        StudentAPIAssignedToTheCourseView.as_view(),
        name='assigned_to_course_list'
    ),
    path(
        'student/unassign_from_course/<int:pk>/',
        StudentAPIUnassignedFromTheCourseView.as_view(),
        name='unassigned_from_course'
    ),
    path(
        'student/report/<int:pk>/',
        StudentAPIPerformanceReport.as_view(),
        name='student_performance'
    )
]
