import csv

from django.http import HttpResponse
from rest_framework import mixins, generics

from course.models import (
    CourseParticipant
)
from student.models import (
    Student
)
from student.serializers import (
    StudentSerializer, AssignStudentToCourseSerializer, StudentAssignedToTheCourseSerializer
)


class StudentAPIView(
    mixins.CreateModelMixin,
    generics.ListAPIView
):
    """
    General view which responsible for getting a students list
    and creating a new student instance

    post:
    Create a new Student instance.
    """
    serializer_class = StudentSerializer

    def get_queryset(self):
        queryset = Student.objects.all()
        return queryset

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class StudentAPIDetailView(
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.RetrieveAPIView,
):
    """
        Detail view which responsible for
        read, update and delete  a Student instance.

        put:
        Update existing Student instance.
        \n

        patch:
        Update existing Student instance.
        \n

        delete:
        Delete existing Student instance.
    """
    serializer_class = StudentSerializer

    def get_queryset(self):
        query_set = Student.objects.all()
        return query_set

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class StudentAPIAssignToCourse(
    generics.CreateAPIView
):
    """
    This view responsible for assign student to course.\n
    Also, it's create a CourseParticipant instance

    post:
    Create a new CourseParticipant instance.
    """
    serializer_class = AssignStudentToCourseSerializer

    def get_queryset(self):
        queryset = CourseParticipant.objects.all()
        return queryset

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class StudentAPIAssignedToTheCourseView(
    generics.ListAPIView
):
    """
    This view responsible for getting list
    already assigned students to course

    """
    serializer_class = StudentAssignedToTheCourseSerializer

    def get_queryset(self):
        queryset = CourseParticipant.objects.all()
        return queryset


class StudentAPIUnassignedFromTheCourseView(
    mixins.DestroyModelMixin,
    generics.RetrieveAPIView,
):
    """
    This view responsible for unassigned student from course.
    Also, it's delete a CourseParticipant instance
    delete:
    Delete existing CourseParticipant instance.
    """
    serializer_class = StudentAssignedToTheCourseSerializer

    def get_queryset(self):
        queryset = CourseParticipant.objects.all()
        return queryset

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class StudentAPIPerformanceReport(
    generics.RetrieveAPIView,
):
    """
    This view responsible for providing a
    report on student performance
    and receiving this information in the csv file

    get:
    Get report about student performance by his pk,
    forms сsv file and download it.
    """

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="report.csv"'

        writer = csv.writer(response)
        for student in Student.objects.filter(pk=self.kwargs['pk']):
            assigned_courses = CourseParticipant.objects.filter(student=student)
            completed_courses = assigned_courses.filter(completed=True)

            headings = (
                "student full Name",
                "number of assigned courses to student",
                "number of completed courses by student"
            )
            rows = (
                student.full_name,
                assigned_courses.count(),
                completed_courses.count()
            )

            writer.writerow(headings)
            writer.writerow(rows)

        return response
