from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from course.models import (
    CourseParticipant
)
from .models import (
    Student
)


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = [
            'id',
            'first_name',
            'last_name',
            'email'
        ]
        validators = [
            UniqueTogetherValidator(
                queryset=Student.objects.all(),
                fields=(
                    'first_name',
                    'last_name'
                ),
                message='This student already exist'
            )
        ]


class AssignStudentToCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseParticipant
        fields = [
            'id',
            'course',
            'student',
        ]
        validators = [
            UniqueTogetherValidator(
                queryset=CourseParticipant.objects.all(),
                fields=(
                    'course',
                    'student'
                ),
                message='This student already assigned to this course'
            )
        ]


class StudentAssignedToTheCourseSerializer(serializers.ModelSerializer):
    student_id = serializers.IntegerField(
        source='student.id',
        read_only=True
    )
    student_name = serializers.StringRelatedField(
        source='student.first_name',
        read_only=True
    )
    course_id = serializers.IntegerField(
        source='course.id',
        read_only=True
    )
    course_name = serializers.StringRelatedField(
        source='course.name',
        read_only=True
    )

    class Meta:
        model = CourseParticipant
        fields = [
            'id',
            'student_id',
            'student_name',
            'course_id',
            'course_name',
        ]
