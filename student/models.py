from django.db import models


class Student(models.Model):
    first_name = models.CharField(verbose_name='students first name', max_length=64, null=False)
    last_name = models.CharField(verbose_name='students last name', max_length=64, null=False)
    email = models.EmailField()

    class Meta:
        db_table = 'student'

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name
