from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from course.models import CourseParticipant, Course
from .models import Student


class StudentAPITestCase(APITestCase):
    def setUp(self):
        student_obj = Student.objects.create(
            first_name='test',
            last_name='student',
            email='test_student@gmail.com',
        )

    def create_item(self):
        url = api_reverse('student:list')
        data = {
            'first_name': 'some_first_name_for_test',
            'last_name': 'some_last_name_for_test',
            'email': 'test_student@gmail.com'

        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Student.objects.count(), 2)
        return response.data

    def test_student_create(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('student:detail', kwargs={'pk': data_id})
        rud_data = {
            'first_name': 'some_new_first_name_for_test',
            'last_name': 'some_new_last_name_for_test'
        }

        get_response = self.client.get(rud_url, rud_data, format='json')
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)

    def test_student_update(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('student:detail', kwargs={'pk': data_id})
        rud_data = {
            'first_name': 'some_new_first_name_for_test',
            'last_name': 'some_new_last_name_for_test',
            'email': 'test_student@gmail.com'
        }

        put_response = self.client.put(rud_url, rud_data, format='json')
        self.assertEqual(put_response.status_code, status.HTTP_200_OK)

    def test_student_delete(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('student:detail', kwargs={'pk': data_id})

        delete_response = self.client.delete(rud_url, data_id, format='json')
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        get_response = self.client.delete(rud_url, format='json')
        self.assertEqual(get_response.status_code, status.HTTP_404_NOT_FOUND)


class StudentAssignToCourseAPITestCase(APITestCase):
    def setUp(self):
        self.student_obj = Student.objects.create(
            first_name='test',
            last_name='student',
            email='test_student@gmail.com',
        )
        self.course_obj = Course.objects.create(
            name='test',
        )

    def create_item(self):
        url = api_reverse('student:assign_student_to_course')
        data = {
            'course': self.course_obj.pk,
            'student': self.student_obj.pk,

        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Student.objects.count(), 1)
        return response.data

    def test_student_unassigned_from_course(self):
        data = self.create_item()
        data_id = data.get('id')
        rud_url = api_reverse('student:unassigned_from_course', kwargs={'pk': data_id})

        delete_response = self.client.delete(rud_url, data_id, format='json')
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)

        get_response = self.client.delete(rud_url, format='json')
        self.assertEqual(get_response.status_code, status.HTTP_404_NOT_FOUND)


class StudentAPIPerformanceReportTestCase(APITestCase):
    def setUp(self):
        self.student_obj = Student.objects.create(
            first_name='test',
            last_name='student',
            email='test_student@gmail.com',
        )
        self.course_obj = Course.objects.create(
            name='test',
        )
        student_obj = CourseParticipant.objects.create(
            course_id=self.course_obj.pk,
            student_id=self.student_obj.pk,
        )

    def test_get_student_performance_report(self):
        data_id = self.student_obj.pk
        rud_url = api_reverse('student:student_performance', kwargs={'pk': data_id})

        get_response = self.client.get(rud_url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)

